# Lemonbar com suporte a fontes XFT no Debian

A `lemonbar` é instalada como uma recomendação do gerenciador de janelas `bspwm`. Infelizmente, a versão upstream da `lemonbar`, que é a versão fornecida pelo Debian, é muito antiga e não tem suporte a fontes XFT. Como o programa é muito pequeno e raramente sofre atualizações do upstream, vale a pena instalar um fork com o suporte a fontes XFT.

<!--more-->

## O que é a 'lemonbar'

Anteriormente chamada apenas de `bar`, a `lemonbar` é um painel super mínimo, leve e customizável para gerenciadores de janelas que não fornecem nenhum por padrão, como o BSPWM, por exemplo. Para ser mais exato, a `lemonbar` é apenas um *"canvas"*, uma janela de painel em branco capaz de exibir informações geradas por qualquer programa que produza uma saída no terminal.

Sendo assim, nós podemos construir os dados que serão exibidos no painel utilizando qualquer linguagem, inclusive em Bash, e é aí que está a principal justificativa do nosso interesse nessa belezinha de software!

## Preparando a instalação

O procedimento é muito simples e rápido. Primeiro, caso o pacote `lemonbar` já esteja instalado, será preciso removê-lo:

```
:~$ sudo apt purge lemonbar
```

Em seguida, se você ainda não tem o programa `git` instalado, a hora é essa:

```
:~$ sudo apt install git
```

O fork da `lemonbar` também irá requerer algumas dependências para a compilação:

```
:~$ sudo apt install build-essential libx11-dev libxft-dev libx11-xcb-dev libxcb-randr0-dev libxcb-xinerama0-dev
```

## Clonando o repositório do fork

É conveniente ter um diretório reservado para organizar as clonagens de repositórios git. No meu caso, eu criei uma pasta chamada `gits` na minha home, e é lá que eu faço as minhas clonagens, mas fique à vontade para dar o nome que você quiser.

Para clonar o repositório:

```
:~$ cd gits
:~/gits$ git clone https://gitlab.com/protesilaos/lemonbar-xft.git
```

## Instalando

Com o repositório clonado, entre na pasta criada no processo e execute os comandos seguintes:

```
:~/gits$ cd lemonbar-xft
:~/gits/lemonbar-xft$ make
:~/gits/lemonbar-xft$ sudo make install
```

## Desinstalando

Caso você queira desinstalar o fork da `lemonbar`, basta entrar novamente na pasta do repositório clonado e executar:

```
:~/gits/lemonbar-xft$ sudo make uninstall
```

## Testando

Se tudo der certo, você será capaz de executar a `lemonbar` informando a fonte da sua preferência, por exemplo:

```
:~$ lemonbar -p -f 'Inconsolata-13' <<< 'Olá, mundo!'
```

## Conclusão

Normalmente, eu não recomendaria a compilação de programas que não estão no repositório do Debian, mas eu não posso ignorar as possibilidades práticas e didáticas da `lemonbar`. Além disso, trata-se de um software livre que sofre poucas atualizações e tem um código fonte facilmente auditável.

Assim que possível, nós veremos como utilizar e desenvolver aplicações para o nosso novo painel preferido!
