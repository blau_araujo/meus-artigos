# Passando listas de pacotes em arquivos para o 'apt'

Muitas vezes nós organizamos os pacotes que costumamos instalar no Debian em arquivos de texto. O problema é que o `apt` não possui um recurso para a ler esses arquivos e nós temos que recorrer ao bom e velho **Bash**.

<!--more-->

O procedimento geral não é complicado: basta expandir o conteúdo do arquivo na linha de comando da instalação, o que é feito facilmente com uma substituição de comandos:

```
:~$ sudo apt install $(cat ARQUIVO)
```

Numa substituição de comandos, a lista de comandos entre os parêntesis é executada e as saídas produzidas são transformadas em argumentos daquilo que queremos executar. Ou seja, antes do `sudo apt install` ser executado, a saída do comando `cat ARQUIVO` substituirá toda a construção representada por: `$(COMANDOS)`.

Por exemplo, digamos que o nosso arquivo (`frutas.txt`) tenha as linhas abaixo:

```
banana
laranja
abacate
pitanga
```

A nossa linha de comando seria algo como:

```
:~$ sudo apt install $(cat frutas.txt)
```

Após a expansão ser processada, e **antes** do `apt` ser efetivamente executado, a nossa linha de comando ficaria assim:

```
:~$ sudo apt install banana laranja abacate pitanga
```

> Você pode verificar esse mecanismo teclando `{Ctrl}`+`{Alt}`+`{e}` antes de executar um comando que contenha uma expansão de parâmetros.

## Dispensando o 'cat'

Se preferir, você pode optar por uma solução 100% Bash:

```
:~$ sudo apt install $(< ARQUIVO)
```

A construção `$(< ARQUIVO)` expande diretamente o conteúdo de `ARQUIVO` na linha de comando, sem a necessidade de recorrer a programas externos (você também pode testar a expansão antes de executar o comando utilizando o atalho acima).

## Indo além

Pessoalmente, eu gosto da ideia de utilizar arquivos para organizar as categorias de pacotes que eu
costumo instalar no Debian, mas nem sempre eu quero instalar todos os pacotes listados nos arquivos. Então, em vez de apagar as linhas que não me interessam (e correr o risco de bagunçar os meus arquivos), eu simplesmente "comento" essas linhas com uma cerquilha (`#`). As linhas comentadas não serão ignoradas pelo `apt`, mas esse procedimento me permite filtrar o que será expandido na linha do comando.

Para demonstrar como a coisa funciona, vamos alterar o nosso arquivo de exemplo:

```
banana
laranja
# abacate
pitanga
```

Para fazer a filtragem, nós vamos recorrer ao utilitário `grep`, construindo uma expressão regular que excluirá a exibição de qualquer linha do arquivo iniciada com o caractere `#`.

Observe:

```
:~$ grep '^[^#]' frutas.txt
banana
laranja
pitanga
```

Onde o primeiro circunflexo (`^`) da expressão regular representa o início da linha e o par de colchetes com o segundo circunflexo é uma lista dos caracteres que não podem aparecer nas linhas que serão comparadas com o padrão. Em outras palavras: se a linha começar com `#`, o `grep` não deverá imprimi-la.

Portanto, o nosso comando final passa a ser:

```
:~$ sudo apt install $(grep '^[^#]' ARQUIVO)
```

Se a ideia for permitir a instalação a partir de vários arquivos, só precisamos de um pequeno ajuste:

```
:~$ sudo apt install $(grep --no-filename '^[^#]' ARQUIVO1 ARQUIVO2 ...)
```

> Sem a opção `--no-filename`, o `grep` imprimiria os nomes dos arquivos junto com os conteúdos das linhas, o que causaria problemas com o `apt`.

## Transformando a solução em script

Para meu uso, eu criei o script abaixo e chamei de `apt-file`:

```
#!/usr/bin/env bash

# Capturando os argumentos
param="$@"
dr=''

# Verificação da opção de simular a instalação...
if [[ $1 == '-s' || $1 == '--dry-run' ]]; then
    dr='--dry-run'
    f="${param/$1/}"
else
    f="$param"
fi

apt install $dr $(grep --no-filename '^[^#]' $f)
```

Para poder executá-lo com `sudo` ou como root, eu o copiei para o diretório `/usr/bin`. Deste modo, quando eu quiser fazer instalações a partir de arquivos com listas de pacotes, basta executar:

```
:~$ sudo apt-file arquivo1 arquivo2 etc...
```

Se eu quiser apenas testar a instalação, basta passar a opção `-s` ou `--dry-run` antes dos nomes dos arquivos (não precisa ser como usuário administrativo):

```
:~$ apt-file -s arquivo1 arquivo2 etc...
```

## Considerações finais

Um recurso tão obviamente útil deveria estar implementado no `apt`, mas o shell, especialmente o Bash, nunca nos deixa na mão. Se você quiser experimentar o script, certifique-se de ter entendido como ele funciona. Não se esqueça de que, para ser executado, o script precisa receber permissões com o `chmod`.

Se você não entendeu o script e não sabe como funciona o `chmod`, é melhor adiar um pouco o seu experimento para depois de fazer o nosso [curso gratuito e livre de programação em Bash](https://debxp.org/cbpb/) ou de me contratar para umas [aulas particulares](https://blauaraujo.com/2021/06/11/aulas-particulares-shell-gnu-linux/).




