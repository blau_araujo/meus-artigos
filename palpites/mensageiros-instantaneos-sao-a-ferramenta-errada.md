# Mensageiros instantâneos são a ferramenta errada!

Sobre o uso do Telegram para abrigar o nosso grupo, eu sempre vi esse tipo de ferramenta como uma porta de entrada para a nossa comunidade. Eu sempre imaginei que, de lá, a gente poderia partir para outras formas de convívio e de construção de conhecimento em plataformas mais adequadas:

<!--more-->

- Um [fórum](https://forum.debxp.org) para dúvidas, suporte e discussões mais importantes: conversas que precisam ser destacadas e preservadas;

- Uma wiki para registrar nossa base de conhecimento e torná-la acessível;

- Um [blog comunitário](https://debxp.org) para a publicação de dicas e de reflexões sobre os assuntos do nosso interesse;

- Uma [sala no IRC](https://web.libera.chat/#debxp) para "jogar conversa dentro" e para a participação em eventos ao vivo: no geral, para conversas informais, daquelas que a gente tira uma horinha no dia pra bater um papo em tempo real com quem mais estiver afim;

Mas nunca consegui convencer gente suficiente de que estamos usando a ferramenta errada para a coisa ir pra frente.

Entendam que, pelo menos em relação à **comunidade debxp**, o objetivo **nunca foi** tornar uma sala/grupo de um mensageiro instantâneo o centro do nosso convívio, a nossa atividade fim. Para mim, Xmpp, Matrix, Telegram, IRC são para a gente dar um "oi" e avisar que algo está acontecendo nas outras plataformas, as que realmente importam.

A dinâmica que eu imaginei e que, pelo visto, só funciona na minha cabeça, é a seguinte:

- Todos que chegam ao grupo do Telegram vão até lá porque conhecem, querem conhecer, ou querem ser notificadas sobre o nosso trabalho: as nossas publicações em vídeo e em texto.

- As conversas mais importantes seriam direcionadas, pelos nossos moderadores e pelos membros mais ativos, para o nosso fórum.

- No fórum, a magia aconteceria: discussões acaloradas sobre tudo que interessa à nossa comunidade, dúvidas técnicas, solução de problemas, discussões de notícias, etc...

- Dessas conversas, os próprios membros atualizariam a nossa wiki para publicizar o conhecimento técnico e demais saberes elaborados nos tópicos do fórum.

- Ou então, no nosso blog colaborativo, as pessoas mais íntimas da arte da escrita publicariam artigos com dicas, tutoriais e reflexões sobre os temas de interesse da comunidade, gerando mais assunto para ser discutido e aprofundado no fórum e mais material para ser publicado na wiki.

Conseguem imaginar aonde daria para chegar com essa dinâmica?

Então, o Telegram em si não é o problema. Não adianta trocar Telegram por Xmpp, Matrix, IRC ou seja lá o que for! O problema é a tendência de se acomodar na comunicação em tempo real, se deixar seduzir pelos números, pelo imediatismo e aceitar, sem qualquer constrangimento, que tudo que nós conversamos por lá se perca.

Fóruns, assim como outras ferramentas do século passado, certamente perderam seu "charme" para a grande massa, e talvez isso não tenha volta, mas ainda são ferramentas úteis e especializadas em seus propósitsos. Além disso, eu acho que a gente chega num ponto onde tem que decidir o que quer: vamos continuar aceitando a perpetuação de uma forma de trabalho volátil, dispersiva, apenas pontual e momentaneamente útil, ou vamos construir algo que possa durar e evoluir, capaz de crescer para muito além dos números de membros?

Essa é a questão.
