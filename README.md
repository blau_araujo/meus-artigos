# Meus artigos

Repositório dos meus artigos sobre assuntos diversos, mas principalmente sobre o shell do GNU/Linux.

![](https://blauaraujo.com/wp-content/uploads/2022/05/cafezinho-01.png)

## Comentários, dúvidas e sugestões

- Utilize as [issues desde repositório](https://codeberg.org/blau_araujo/meus-artigos/issues)

## Como apoiar o meu trabalho

* Doações via PIX: pix@blauaraujo.com
* [Doações pelo PicPay](https://app.picpay.com/user/blauaraujo)
* [Apoio mensal pelo Apoia.se](https://apoia.se/debxpcursos)

## Cursos livres e gratuitos

* [Curso Básico de Programação em Bash](https://codeberg.org/blau_araujo/prog-bash-basico)
* [Curso intensivo de programação do Bash (em vídeo)](https://youtube.com/playlist?list=PLXoSGejyuQGr53w4IzUzbPCqR4HPOHjAI)
* [Curso Shell GNU](https://codeberg.org/blau_araujo/o-shell-gnu)
* [Curso de expressões regulares](https://codeberg.org/blau_araujo/regex-shell)
* [Curso de interfaces para scripts em shell (em andamento)](https://codeberg.org/blau_araujo/interfaces-para-scripts)
