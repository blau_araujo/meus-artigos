# As coisas que te ensinaram errado sobre (ponha um tema aqui)

Entre as várias coisas que me incomodam demais na web, estão os títulos depreciativos de certos artigos e vídeos, redigidos com o claro propósito de chamar a nossa atenção (os *click baits*), causar polêmicas ("tretas") ou insinuar que seus autores estão em algum tipo de patamar superior aos demais mortais (*guruismo*, como se dizia antigamente).

<!--more-->

Neste artigo, a ideia não é condenar pessoas, mas alertá-las sobre as mensagens implícitas nessas abordagens, pelo menos segundo a minha percepção, muitas vezes adotadas de boa fé, sem qualquer reflexão mais profunda ou segundas intenções. Aliás, gente muito boa utiliza esses títulos em tom de brincadeira ou como referências a elementos da cultura popular. Contudo, mesmo nesses casos, eu creio que são abordagens contraproducentes, até em termos didáticos, se o objetivo sincero for compartilhar saberes livremente.

## Os títulos

Eu me refiro, especialmente, a títulos como:

- *"Tudo que você aprendeu errado sobre tal coisa"*
- *"Como se tornar um ninja disso ou daquilo"*
- *"Pare de usar tal estrutura da linguagem X"*

Entre tantos outros que, frequentemente (nem sempre), quando vamos assistir aos vídeos ou, mais raramente nos nossos dias, ler os artigos, só encontramos apresentações superficiais e cheias de erros conceituais importantes. Vez ou outra, acontecem exceções quanto ao conteúdo, mas isso não atenua em nada a depreciação e a arrogância implícitas nessas abordagens em si.

Senão, vejamos...

### Tudo/coisas que você aprendeu errado...

Esse tipo de título, mesmo em tom de brincadeira, traz implícita a ideia de que você é um idiota preguiçoso, que não se preocupou em aprender direito os conceitos do ramo do seu interesse ou, de outro lado, que suas fontes (professores, livros, etc) foram, no mínimo, incompetentes. Ainda que tudo isso sobre nós, o público, e nossas fontes fosse verdade, qual o ganho de anunciar um vídeo ou artigo desta forma?

Para a audiência, nenhum! Porém, para quem publica, é uma oportunidade de alegar autoridade com base em um conhecimento supostamente acima da média: isso quando os autores não se colocam explicitamente como os únicos detentores da verdade *do universo, da vida e tudo mais*.

Mas a abordagem impressiona pelo espetáculo mágico, pelo tom de revelação quase mística, e acaba por facilitar, como muito frequentemente almejado, a construção de um rebanho de seguidores *embasbacados* com o trivial, o óbvio e a confortável noção de que não é necessário estudar e pensar para se tornar um especialista: é muito mais fácil delegar a terceiros a função de pensar por nós.

### Como se tornar um ninja...

Novamente: pode ser apenas brincadeira, uma forma divertida de abordar algum assunto, mas é importante ficarmos atentos ao nosso senso de humor. O ponto é que a mensagem implícita nessa abordagem não é muito diferente da anterior, só que pior, pois coloca um foco enorme numa promessa: a de que você será um ninja mítico da sua área com pouco esforço -- basta aceitar o autor como seu *sensei* e seus *senjutus* como a verdade.

No fim das contas, ambas as abordagens têm um objetivo muito claro: **criar dependência**. Você não precisa aprender nada, ou melhor, você é **incapaz** de aprender qualquer coisa, a não ser comprando o próximo curso, "mentoria", *coaching* e conteúdos exclusivos, convenientemente protegidos de qualquer questionamento atrás de comentários bloqueados, muralhas de pagamento e direitos de cópia.

Deste modo, as verdades do santo *sensei* podem seguir irrefutáveis e você poderá continuar acreditando que tem a vantagem competitiva do acesso a conhecimentos que ninguém mais possui. Infelizmente, fora dos animes e mangás, problemas não se resolvem com *ninjutsus*. Os tais *jutsus*, na vida real, não passam de truques -- e, diante dos problemas, nós precismos de métodos:

> "Um truque é uma boa ideia que resolve um problema; um método é uma boa ideia que resolve muitos problemas." ([Prof. Augusto Morgado](https://pt.wikipedia.org/wiki/Augusto_C%C3%A9sar_de_Oliveira_Morgado))

### Pare de usar a estrutura da linguagem X...

A megalomania atinge as alturas com títulos assim! Não há promessas nessa abordagem, porque você simplesmente será incapaz de alcançar o grau de sabedoria do autor, geralmente um jovem com menos de 30 anos, recém-formado em alguma faculdade de classe média alta e com menos de 3 anos de experiência no ramo (os mesmos que costumam oferecer "mentorias").

No geral, autores de vídeos e artigos apresentados desta forma enquadram-se muito bem na categoria dos *"cagadores de regras"*: pessoas que colocam regras acima do pensamento contextualizado, crítico e autônomo. São pessoas inseguras, controladoras e que transitam muito bem no reino das "tretas" e "lacrações", ou seja: ambientes onde qualquer reflexão é impossível e qualquer tentativa de diálogo ou questionamento é imediatamente barrada.

Neste caso, a visibilidade e o "engajamento", como se diz hoje em dia, não são conquistados pelo conteúdo (até porque o título é suficiente para provocar as reações esperadas), mas pela polêmica. Assim, mais uma vez, não há o menor interesse por compartilhar saberes, aprofundar temas ou acompanhar o aprendizado de alguém: tudo que importa é a autopromoção, ainda que pela discórdia.

## Afirmações extraordinárias

Como dizia Carl Sagan: *"afirmações extraordinárias requerem evidências extraordinárias"*. Concordando com ele, eu quero dizer que, se você não estiver à altura de demonstrar aquilo que afirma nos seus títulos, o melhor é reduzir suas ambições de visibilidade e monetização e ser honesto com o que pretende compartilhar. Isso, ou esperar até que seus argumentos e a sua base conceitual sejam capazes de fundamentar suas alegações.

A demonstração é o que diferencia "um título arrogante para um conteúdo ruim" de "um ótimo conteúdo com um título ruim". No fim das contas, não é o título que testemunha ou nega a sua boa fé, mas o seu conteúdo, e eu não tenho nem palavras para dizer o quanto isso é relevante.

## O outro lado da moeda

A verdade é que nenhuma dessas abordagens seria bem-sucedida se não fosse pelo público: as visualizações e *likes* mostram que boa parte das audiências prefere ser enganada ou é programada para aceitar como natural a enganação. A enganação é confortável, empodera o ignorante, confirma suas crenças, não mexe com seu ego, não o obriga a se esforçar e faz com que os enganadores prosperem e se multipliquem.

Enganar é um bom negócio, está 100% em harmonia com as demandas do mercado, mas ninguém vende nada se não houver quem compre: e todos nós, em um aspecto ou outro da vida, estamos dispostos a comprar ilusões. Existem muitas pressões para que as coisas sejam assim, mas passou da hora de despertarmos coletivamente.

Não existe um *jutsu de dissipação* para isso, mas nós podemos trabalhar juntos persistindo em pequenas atitudes...

### Aprendendo a evitar enganadores

- Pelas abordagens expressas nos títulos.
- Pela promessa de resultados fáceis e rápidos.
- Recusando-nos a deixar que alguém pense por nós.
- Recusando e silenciando convites para tretas.
- Deixando *senseis* para os mundos de fantasia dos animes e mangás.

### Treinando o pensamento crítico e autônomo

- Preferindo a contextualização às regras genéricas.
- Preferindo a informação ao espetáculo.
- Preferindo perguntas e questionamentos às *lacrações*.
- Aceitando, de uma vez por todas, que truques mágicos não resolvem problemas.
- Exigindo demonstrações de afirmações categóricas e extraordinárias.

### Valorizando e divulgando conteúdos honestos

- Dando relevância na web com curtidas e (principalmente) compartilhamentos.
- Preferindo conteúdos publicados sob licenças livres e questionando os autores sobre esse "detalhe".
- Preferindo conteúdos que incentivam e estimulam o nosso questionamento.
- Preferindo cursos e conteúdos que visam a nossa autonomia e só prometem que teremos que praticar muito.
- Compreendendo que bons conteúdos e cursos tomam tempo, exigem pesquisa e vêm com custos pessoais e financeiros para os autores muito maiores do que eles costumam pedir em troca.

## Conclusão

Como eu disse, a ideia não é condenar ninguém em específico nem dar indiretas. A questão aqui é tornar evidente algumas das formas pelas quais nós somos induzidos ao erro ou, talvez sem querer, nós passamos impressões erradas sobre os nossos propósitos.

Falando apenas por mim, eu acredito que essa reflexão é necessária e até urgente. Mesmo as minhas análises sobre as abordagens que me incomodam podem não passar disso: incômodos derivados de impressões equivocadas da minha parte, daí a importância da sua participação nessa reflexão.

Tudo que eu peço é: pense nisso.
