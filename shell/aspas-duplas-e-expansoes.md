# Aspas duplas e expansões

Dúvida apresentada na issue [#1](https://codeberg.org/blau_araujo/tecnicas-do-shell/issues/1) do curso [Técnicas do Shell](https://blauaraujo.com/2022/03/12/curso-permanente-tecnicas-do-shell/).

<!--more-->

## Pergunta

*Uma observação com aspas duplas, não é porque ela tem este "poder" de expansão que deve estar em tudo, mas que o entendimento que, a expansão se dá pelo fato de que ela será expandida porque já foi criada como especial. Estou certo?*

## Resposta

O que a gente chama de "especial" (como em "significado especial"), é determinado pelo próprio shell: são os chamados *tokens* ou, traduzindo, **componentes léxicos**. Simplificando, são os elementos que compõe aquilo que podemos chamar de "linguagem do shell".

No caso das aspas duplas, o importante é lembrar que elas têm o exato comportamento das aspas simples: tudo ali dentro é caractere literal, mas com quatro exceções: o caractere cifrão (`$`), o acento grave, a contra-barra (`\`) e a exclamação (`!`).

Essas quatro exceções só serão exceções se vierem na forma de componentes léxicos:

| Caractere | Componente léxico |
|---|---|
| `$` | Será um *token* se vier antes de um identificador de variáveis válido (`$var` ou `${var}` - expansão de parâmetros), antes de parêntesis simples( `$(COMANDO)` - substituição de comandos) ou antes de parêntesis duplos (`$((EXPRESSÃO))` - expansão aritmética). Em qualquer outra situação, não forma um *token*. |
| Acento grave | Sempre será um componente léxico: a forma obsoleta e não recomendada da substituição de comandos. |
| `\` | Só será um componente léxico se vier antes de `$` ou do acento grave. |
| `!` | No **modo interativo** (terminal), busca uma ocorrência da palavra que vier em seguinda no histórico. |

Veja os exemplos:


### Cifrão

```
:~$ echo "banana $USER $ laranja"
banana blau $ laranja
            ↑
      não é um token
```

### Contra-barra

```
:~$ echo "banana \$USER \$ \laranja"
banana $USER $ \laranja
       ↑     ↑ ↑
       |     | |
       +--+--+ +--- é só uma \
          |
       escapou
```

### Acento grave

```
:~$ echo "Meu nome é `whoami`"
Meu nome é blau

             acento sobrando...
                   ↓
:~$ echo "Meu nome ` é `whoami`"
>  <--- comando incompleto!

:~$ echo "Meu nome \` é `whoami`"
Meu nome ` é blau
         ↑
      escapado
```

### Exclamação

```
            busca no histórico
                   ↓
~ $ echo "Eu curto !framboesa."
bash: !framboesa.: event not found

                     só uma exclamação
                            ↓
~ $ echo "Eu curto framboesa!"
Eu curto framboesa!


~ $ echo "Eu curto \!framboesa"
Eu curto \!framboesa
         ↑
 a contra-barra só escapa $ e `,
 mas impede a busca no histórico!
```

### Dica

Eu sei que é sempre mais interessante quando conseguimos explicar com as nossas próprias palavras, mas é melhor ainda quando conseguimos **não fugir dos conceitos**. Então, em vez de me perguntar *"estou certo?"*, tente entender a minha escolha de palavras e perguntar por que eu escolhi apresentar os conceitos de determinada forma. ;-)

### Em tempo...

As aspas duplas **não permitem expansões!** Elas só não tornam literais os caracteres `$` e acento grave que, não por acaso, participam de apenas alguns tipos de expansões.

Til (`~`), caracteres coringa (`*`, `?` e `[...]`) e sequências entre chaves (`{...}`) também são expansões, mas **não são exceções dentro de aspas duplas**.
