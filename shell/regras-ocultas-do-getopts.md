# As regras ocultas do 'getopts'

Ao contrário da opinião geral, eu considero o `getopts` -- um *builtin* do Bash para processar argumentos nomeados -- uma excelente ferramenta! Contudo, os aspectos mais importantes de seu uso prático estão pessimamente documentados (quando estão) e raramente são abordados de forma adequada em postagens na web. <!--more-->Neste artigo, eu pretendo antecipar alguns problemas que podemos enfrentar quando não conhecemos as *regras ocultas* do `getopts`, mas, principalmente, esclarecer como ele realmente funciona.

## Uso geral

Para compreender a origem das tais "regras ocultas" a que me refiro, é preciso compreender o funcionamento geral do `getopts`. Aliás, entendendo o funcionamento, as regras deixam de ser "regras" e deixam de ser "ocultas"! Então, observe a sintaxe geral do `getopts`:

```
getopts [:]STRING_DAS_OPÇÕES VAR LISTA_DE_ARGUMENTOS
```

Onde:

| Argumento             | Descrição                                                                                                                                                              |
|-----------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `:`                   | Silencia os erros do shell e utiliza os caracteres especiais do `getopts` para tratamento de erros.                                                                    |
| `STRING_DAS_OPÇÕES`   | A lista das letras que serão processadas como opções.                                                                                                                  |
| `VAR`                 | Uma variável que armazenará a opção que estiver sendo processada no momento.                                                                                           |
| `LISTA_DE_ARGUMENTOS` | Quando não informada, o `getopts` lê todos os argumentos passados na linha do comando que invocou o script, mas nós podemos expandir qualquer outra lista de palavras para esta finalidade. |

Cada vez que for invocado, `getopts` lerá uma palavra da `LISTA_DE_ARGUMENTOS`. Se a palavra iniciar com traço (`-`) e corresponder a uma das letras definidas na `STRING_DAS_OPÇÕES`, a palavra será passada para `VAR` (sem o traço) e `getopts` emitirá um estado de saída de sucesso (`0`).

> **Nota:** se você quiser testar os exemplos a seguir, será preciso reiniciar a sessão do shell antes de cada um deles com o comando: `exec bash` ou reiniciando a variável `OPTIND` com o valor `1`: `OPTIND=1`.

Observe:

```
:~$ getopts 'abc' var -a -b -c; echo $? $var
0 a
:~$ getopts 'abc' var -a -b -c; echo $? $var
0 b
:~$ getopts 'abc' var -a -b -c; echo $? $var
0 c
```

Se não houver mais palavras para serem lidas, `getopts` carregará o carectere `?` em `VAR` e emitirá um estado de saída de erro (`1`):

```
:~$ getopts 'abc' var -a -b -c; echo $? $var
1 ?
```

Se uma das palavras na lista não iniciar com um traço, a partir deste momento, `getopts` sempre emitira um estado de erro (`1`) e atribuirá `?` à variável `VAR`:

```
:~$ getopts 'abc' var -a b -c; echo $? $var # -a: ok, inicia com traço.
0 a
:~$ getopts 'abc' var -a b -c; echo $? $var # b: opa, não inicia com traço, é erro!
1 ?
:~$ getopts 'abc' var -a b -c; echo $? $var # tudo que vem depois resulta em erro.
1 ?
```

Por outro lado, se a palavra iniciar com traço, mas não constar da string de opções, `getopts` sairá com sucesso (`0`) e atribuirá o caractere `?` à variável `VAR`:

```
:~$ getopts 'abc' var -a -d -b -c; echo $? $var
0 a
:~$ getopts 'abc' var -a -d -b -c; echo $? $var # Opa! '-d' não foi convidada!
bash: opção ilegal -- d                         # Mensagem de erro do Bash.
0 ?                                             # Mas, segue a fila...
:~$ getopts 'abc' var -a -d -b -c; echo $? $var
0 b
:~$ getopts 'abc' var -a -d -b -c; echo $? $var
0 c
:~$ getopts 'abc' var -a -d -b -c; echo $? $var
1 ?
```

### Percorrendo argumentos em loop

Obviamente, nós não escreveremos o comando do `getopts` toda vez que quisermos avançar uma palavra na lista de argumentos. Para isso, o mais comum é utilizarmos o comando composto `while`: um loop condicionado ao estado de saída de um comando testado:

```
:~$ while getopts 'abc' var -a -b -c; do echo $? $var; done
0 a
0 b
0 c
```

Observe que o loop foi encerrado assim que `getopts` saiu com erro, o que não aconteceria em função de uma opção inválida:

```
:~$ while getopts 'abc' var -a -d -b -c; do echo $? $var; done
0 a
bash: opção ilegal -- d
0 ?
0 b
0 c
```

### Ocultando a mensagem de erro

Para silenciar as mensagens de erro do shell, o primeiro caractere da string de opções deve ser `:`...

```
:~$ while getopts ':abc' var -a -d -b -c; do echo $? $var; done
0 a
0 ?
0 b
0 c
```

### Argumentos obrigatórios

Algumas opções podem ser definidas de forma a requerem um argumento adicional. Para isso, basta incluir o caractere `:` logo após a letra correspondente: se o argumento adicional for informado na linha do comando, ele será armazenado na variável especial `OPTARG`.

Observe:

```
:~$ while getopts ':ab:c' var -a -b banana -c; do echo $? $var $OPTARG; done
0 a
0 b banana
0 c
```

Caso o argumento obrigatório não seja informado, o caractere `:` será atribuído à variável `VAR`:

```
:~$ while getopts ':ab:c' var -b; do echo $? $var $OPTARG; done
0 : b
```

#### Mas, cuidado!

**Qualquer palavra encontrada depois de uma opção com argumento obrigatório será tratada como o argumento requerido!**

```
:~$ while getopts ':ab:c' var -a -b -c; do echo $? $var $OPTARG; done
0 a
0 b -c    # Não era isso que eu queria!
```

**Detalhe:** este comportamento não é evitável diretamente pelo comando `getopts` e, se precisarmos de uma opção que receba um argumento opcional (`-b ARG` ou apenas `-b`, por exemplo), a lista de palavras terá que passar por algum tipo de tratamento no script antes de ser passada para `getopts`.

Um exemplo para ilustrar a ideia:

```
:~$ lista="-a -b -c"
:~$ lista=${lista/-b -/-b X -}
:~$ while getopts ':ab:c' var $lista; do echo $? $var $OPTARG; done
0 a
0 b X    # Agora temos um OPTARG padrão para trabalhar!
0 c
```

Testando o método com um argumento passado para `-b`:

```
:~$ lista="-a -b banana -c"
:~$ lista=${lista/-b -/-b X -}
:~$ while getopts ':ab:c' var $lista; do echo $? $var $OPTARG; done
0 a
0 b banana   # A substituição do tratamento não foi feita.
0 c
```

> **Nota:** o maior problema dos argumentos obrigatórios é que, como visto nos exemplos acima, não podemos contar com o caractere `:` em `OPTARG` para fazer algum tratamento de erro. Se esta for uma preocupação de projeto, se você realmente acha que precisa garantir que o utilizador informe o argumento de uma opção obrigatória causando a interrupção do script, você terá que tratar os argumentos da linha do comando antes de utilizar o `getopts`.

## Opções nomeadas e argumentos

Como vimos, palavras na lista de argumentos que não iniciam com traço (`-`) fazem com que o `getopts` emita um estado de erro (`1`). Isso é muito preocupante, porque força o utilizador a informar argumentos adicionais depois das opções:

```
:~$ while getopts ':ab:c' var -a -b pera -c uva manga; do echo $? $var $OPTARG; done
0 a
0 b pera
0 c
```

Os argumentos `uva` e `manga` ainda estão na lista de palavras, mas não poderão ser acessadas durante o loop que processa as opções de `getopts`, o que nos leva a duas abordagens possíveis:

1. **Antes do loop:** tratar a lista de palavras separando as opções dos argumentos.
2. **Depois do loop:** padronizar a linha do comando, de modo que os argumentos estejam sempre depois das opções, e deslocar (`shift`) os parâmetros posicionais para remover todos os argumentos já processados.

### Abordagem 1: separação dos argumentos

Mais comumente, a lista de argumentos é recebida pelo `getopts` através da expansão da [variável especial `@`](https://blauaraujo.com/curso-shell-gnu/aula-07-expansoes-de-parametros/) entre aspas duplas, o que acontece escrevendo explicitamente `"$@"` ou simplesmente omitindo o terceiro parâmetro do `getopts`, o efeito é o mesmo:

```
# Expandindo '@' explicitamente:
getopts ':ab:c' var "$@"

# Omitindo o terceiro parâmetro:
getopts ':ab:c' var
```

Se quisermos tratar os argumentos recebidos, nós precisaremos capturar o conteúdo de `"$@"` e, de acordo com cada caso, separar as opções nomeadas (e seus respectivos argumentos obrigatórios, se houver) dos demais argumentos.

No caso do exemplo que temos utilizado até agora, um possível tratamento no script (`teste.sh`) seria este:

```

# Preparando os vetores de opções e de argumentos...

declare -a opt_list
declare -a arg_list

# Percorrendo o vetor 'args' para capturar as opções
# e os argumentos separadamente...

for ((i=0; i<${#args[@]}; i++)); do
    # Iniciando a variável temporária 'opt'...
    opt=''
    # Opções com argumentos obrigatórios...
    case ${args[i]} in
	'-b') # A opção com argumento obrigatório
	      opt=${args[i]}
	      if [[ ${args[i+1]::1} = '-' ]]; then
		  # Argumento obrigatório não informado...
		  opt+=' UM VALOR PADRÃO QUALQUER'
	      else
		  # Argumento obrigatório informado...
		  opt+=" ${args[i+1]}"
		  ((i++))
	      fi
	      opt_list+=("$opt")
	      ;;
          -?) # Opções que não são '-b' ou números negativos...
              opt_list+=(${args[i]})
	      ;;
           *) # Demais argumentos...
              arg_list+=(${args[i]})
    esac
done

# Testes...

echo Opções:
printf '%s\n' "${opt_list[@]}"
echo Argumentos:
printf '%s\n' "${arg_list[@]}"
```

O que resultaria em:

```
:~$ ./teste.sh -a -b pera -c uva manga
Opções:
-a
-b pera
-c
Argumentos:
uva
manga

:~$ ./teste.sh uva manga -a -b pera -c
Opções:
-a
-b pera
-c
Argumentos:
uva
manga
```

Na linha do `getopts`, os vetores das listas seriam informados como:

```
getopts ':ab:c' var ${opt_list[@]} ${arg_list[@]}
```

Como podemos ver, flexibilizar o modo como o utilizador passará os argumentos dificilmente será uma tarefa trivial. Além disso, a solução ficará fortemente atrelada ao caso de uso e, consequentemente, todo o propósito de utilizar o `getopts` deixa de fazer sentido, visto que, durante a própria separação de opções e argumentos, nós já poderíamos decidir o que fazer com os parâmetros recebidos.

> **Em outras palavras:** se somos obrigados a tratar os parâmetros passados pelo utilizador na linha do comando, não há por que utilizar o `getopts`.

### Abordagem 2: padronizar a linha do comando

Nesta abordagem, nós estabelecemos como o nosso script será utilizado, documentamos cuidadosamente a forma de uso e deixamos que o utilizador descubra, ele mesmo, o que acontece no caso de uso incorreto. Infelizmente, porém, o `getopts` não nos dá muitos "ganchos" para o tratamento de erros e elaboração de mensagens: tudo se limita a testar os caracters recebidos em `VAR` (inclusive os caracteres `?` e `:`) e os valores em outras variáveis, como `OPTARG` e `OPTIND`.

### Possíveis argumentos iniciados com traço (`-`)

Mesmo com a padronização, observe o cenário abaixo:

```
:~$ while getopts ':ab' var -a -b $((10-20)); do echo $? $var $OPTARG; done
0 a
0 b
0 ? 1
0 ? 0
```

Perceba que a expansão aritmética `$((10-20))` resultou em `-10` na linha do comando, o que fez com que `getopts` visse uma opção não informada na string de opções (`:ab`). Nós podemos evitar este problema separando as opções dos argumentos com `--`:

```
:~$ while getopts ':ab' var -a -b -- $((10-20)); do echo $? $var $OPTARG; done
0 a
0 b
```

Funciona perfeitamente, mas, novamente, o uso do script teria que estar muito bem documentado para que o utilizador soubesse que pode separar opções de argumentos com `--`.

## Acessando o restante dos argumentos

Até aqui, nós vimos o que acontece com o processamento das opções definidas no `getopts`. Em casos reais de uso, o mais comum é que a lista de argumentos seja obtida a partir da expansão de `@`, que é uma variável especial que armazena todos os parâmetros recebidos pela linha do comando que invoca o nosso script. Para simular esta situação nos nossos próximos exemplos, eu vou utilizar o comando `set -- PARÂMETROS_POSICIONAIS` no terminal:

```
:~$ set -- -a -b banana laranja
:~$ echo "$@"
-a -b banana laranja
```

Portanto, todos os próximos exemplos presumirão que esta será a expansão de `@`. Sendo assim, observe novamente o que acontece no loop do `getopts`:

```
:~$ while getopts ':ab' var; do echo $? $var $OPTARG; done
0 a
0 b
```

Aqui, apenas as opções encontradas em `"$@"` foram lidas e processadas, mas os demais parâmetros capturados ainda estão lá para serem usados:

```
:~$ while getopts ':ab' var; do echo $? $var $OPTARG; done
0 a
0 b
:~$ echo "$@"
-a -b banana laranja
```

O ideal, porém, seria que apenas os parâmetros não processados restassem depois do loop, mas isso depende de um pequeno processamento que pode ser feito com o comando interno `shift` e a variável do shell `OPTIND`.

### Entendendo o 'shift'

O comando `shift` remove `N` parâmetros do início da lista de parâmetros posicionais. Por padrão, a quantidade `N` é `1`, então:

```
:~$ echo $@
-a -b banana laranja

:~$ shift
:~$ echo $@
-b banana laranja

:~$ shift
:~$ echo $@
banana laranja

:~$ shift
:~$ echo $@
laranja

:~$ shift
:~$ echo $@

:~$
```

### A variável 'OPTIND'

Já a variável `OPTIND` é o índice da última opção processada pelo `getopts`. Quando a sessão do shell é iniciada, `OPTIND` tem o valor `1`:

```
:~$ echo $OPTIND
1
```

Isso informa ao `getopts` que ele deve iniciar o processamento a partir do parâmetro de índice `1`. À medida em que as opções são processadas, `getopts` atualiza `OPTIND` para o índice do parâmetro seguinte:

```
~/tmp $ while getopts ':ab' var; do echo $? $var $OPTARG $OPTIND; done
0 a 2
0 b 3
```

Deste modo, no nosso exemplo, o último parâmetro processado foi o de índice `2`, mas o valor atual de `OPTIND` é `3`. O mais imortante, porém, é que nós temos como saber quantos parâmetros já foram processados e poderão ser descartados da lista de parâmetros posicionais com o `shift`: a quantidade de parâmetros a remover será: `OPTIND-1`:

```
:~$ echo $OPTIND
1

:~$ while getopts ':ab' var; do echo $? $var $OPTARG $OPTIND; done
0 a 2
0 b 3

:~$ echo "$@"
-a -b banana laranja

:~$ shift $((OPTIND-1))
:~$ echo "$@"
banana laranja
```

## As tais regras ocultas do 'getopts'

Muitas vezes, por não entendermos como algum comando ou recurso do shell funciona, nós acabamos inferindo seus comportamentos como se fossem "regras". Por outro lado, quando os conceitos são testados e compreendidos, nós adquirimos pleno controle na decisão sobre como utilizar determinado recurso nos nossos projetos.

Então, conhecendo melhor como o `getopts` funciona, aqui estão alguns pontos que precisam ser levados em conta a respeito de seu uso:

1. Se você pretende receber apenas *flags* (opções na forma de uma letra antecedida por um traço) pela linha de comando, não há no que pensar: pode contar com o `getopts`.

1. Se você quer receber apenas opções com argumentos obrigatórios, o `getopts` também é uma ótima escolha (ainda será fácil tratar eventuais erros de uso).

1. Se você quer receber *flags* e opções com argumentos obrigatórios, o `getopts` ainda é uma boa alternativa, mas o seu script estará mais vulnerável aos usos incorretos por parte do utilizador.

1. Se você quer receber *flags*, opções com argumentos obrigatórios e outros tipos de argumentos, o trabalho de pré-processar os parâmetros posicionais pode tornar o `getopts` de pouca ou nenhuma serventia no seu script.

> Como podemos ver, as tais "regras" não passam de decisões de projeto que precisarão ser tomadas.

Muitos criticam o `getopts` por ser difícil de usar, confuso, limitado, mas nada disso se justifica. O único problema que ele tem é o mesmo de quase todos os comandos mais avançados do shell: documentação incompleta ou mal escrita. No fim das contas, o `getopts` faz aquilo que foi feito para fazer e faz muito bem! Se você puder dedicar um tempinho para conhecê-lo, pode ter certeza, o seu "cinto de utilidades" ficará bem mais poderoso e o seu repertório do shell será muito mais rico.


